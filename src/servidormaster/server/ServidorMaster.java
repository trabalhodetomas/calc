package servidormaster.server;

import base.server.AbstractServidor;
import servidormaster.conn.ConexaoSlave;
import servidormaster.conn.RoundRobinLogic;

import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Pedro Felipe on 19/04/2017.
 */
public class ServidorMaster extends AbstractServidor implements Runnable {

    private List<ConexaoSlave> basicos;
    private List<ConexaoSlave> especiais;

    private List<SocketMaster> clientes;

    private RoundRobinLogic rr_basicos;
    private RoundRobinLogic rr_especiais;

    //private ExecutorService executorServiceBasico;
    //private ExecutorService executorServiceEspecial;
    private ExecutorService executorService;
    private boolean canRun = true;

    public
    ServidorMaster(int porta, List<ConexaoSlave> basicos,  List<ConexaoSlave> especiais ) throws IOException {

        super(porta);
        //executorServiceBasico = Executors.newFixedThreadPool(2);
        //executorServiceEspecial = Executors.newFixedThreadPool(2);
        executorService = Executors.newCachedThreadPool();
        this.basicos = basicos;
        this.especiais = especiais;

        rr_basicos = new RoundRobinLogic(basicos);
        //executorServiceBasico.execute(rr_basicos);
        rr_especiais = new RoundRobinLogic(especiais);
        //executorServiceEspecial.execute(rr_especiais);



        clientes = new ArrayList<>();
        iniciaServer();

    }


    @Override
    public void run() {
        System.out.println("Servidor Master no ar..");
        while(canRun){
            Socket cliente = null;
            try {
                System.out.println("Aguardando conexao...");
                cliente = getServerSocket().accept();

                System.out.println("Cliente conectado: " + cliente.getRemoteSocketAddress());
                SocketMaster socket = new SocketMaster(cliente, rr_basicos, rr_especiais);
                clientes.add(socket);

                executorService.execute(socket);


            } catch (Exception e) {
                canRun = false;
            }
        }
    }

    @Override
    public void closeServer() throws IOException {
        for(SocketMaster cliente : clientes){
            cliente.close();
        }

        for(ConexaoSlave slave  : basicos){
            slave.closeConn();
        }
        for(ConexaoSlave slave  : especiais){
            slave.closeConn();
        }

        if(executorService != null){
            executorService.shutdown();
        }

//        if(executorServiceBasico != null){
//            executorServiceBasico.shutdown();
//        }
//
//        if(executorServiceEspecial != null){
//            executorServiceEspecial.shutdown();
//        }
        canRun = false;

        super.closeServer();


    }


}
