package servidormaster.server;

import base.exception.ConexaoRecusadaException;
import base.util.VerificaCalculo;
import servidormaster.conn.ConexaoSlave;
import servidormaster.conn.RoundRobinLogic;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Iterator;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by Pedro Felipe on 19/04/2017.
 */
public class SocketMaster implements Runnable {

    private Socket socket;

    private RoundRobinLogic rr_basicos;
    private RoundRobinLogic rr_especiais;

    private DataInputStream in;
    private DataOutputStream out;

    private ExecutorService executorServiceBasico;
    private ExecutorService executorServiceEspecial;

    public SocketMaster(Socket socket, RoundRobinLogic rr_basicos, RoundRobinLogic rr_especiais ) throws IOException{
        this.socket = socket;
        executorServiceBasico = Executors.newFixedThreadPool(1);
        executorServiceEspecial = Executors.newFixedThreadPool(1);
        this.rr_basicos = rr_basicos;
        this.rr_especiais = rr_especiais;
        executorServiceBasico.execute(rr_basicos);
        executorServiceEspecial.execute(rr_especiais);

    }

    public String recebe() throws IOException{
        String r = "";
        if(!socket.isClosed()) {
        in = new DataInputStream(socket.getInputStream());
        r = in.readUTF();
        }
        return r;
    }

    public void envia(String msg) throws IOException{
        if(!socket.isClosed()) {
            out = new DataOutputStream(socket.getOutputStream());
            out.writeUTF(msg);
        }
    }

    @Override
    public void run() {
        try {
            String calculo = "";
            while(socket.isConnected() && !calculo.equalsIgnoreCase("Sair")) {

                calculo = recebe();
                System.out.println(calculo);
                if(calculo.equalsIgnoreCase("Teste")){
                    envia("OK");
                }else{
                    if (!calculo.isEmpty() && !calculo.equalsIgnoreCase("Sair"))
                        envia(processar(calculo));
                }

            }
            if(calculo.equalsIgnoreCase("Sair")) {
                close();
                System.out.println("Cliente " + socket.getRemoteSocketAddress() + " Saiu!");
            }
        } catch (IOException e) {
            e.printStackTrace();
            throw new ConexaoRecusadaException("Não foi possivel responder sua solicitação!");
        }

    }

    private String processar(String calculo) throws IOException {
        String operador = VerificaCalculo.getOperador(calculo);
        String resultado = null;
        ConexaoSlave conexaoSlave;
        if(operador.equals("+") ||
                operador.equals("-") ||
                operador.equals("*") ||
                operador.equals("/")){

            conexaoSlave = rr_basicos.getOutput().iterator().next();
            executorServiceBasico.execute(rr_basicos);
        }else{

            conexaoSlave = rr_especiais.getOutput().iterator().next();
            executorServiceEspecial.execute(rr_especiais);
        }

        if(conexaoSlave != null){
            //conexaoSlave.enviar(calculo);
            while (resultado == null) {
                if (!calculo.isEmpty()) {
                    conexaoSlave.enviar(calculo);
                }
                resultado = conexaoSlave.receber();
            }
        }
        return resultado;
    }

    public void close() throws IOException {
        executorServiceBasico.shutdown();
        executorServiceEspecial.shutdown();
        envia("Sair");

        in.close();
        out.close();
        socket.close();
    }
}
