package servidormaster.controller;

import base.exception.PortaVaziaException;
import servidormaster.conn.ConexaoSlave;
import servidormaster.server.ServidorMaster;
import servidorslave.controller.ServidorSlaveController;
import servidorslave.server.ServidorSlave;

import javax.swing.*;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by INSS on 24/04/2017.
 */
public class ServidorMasterController {


    private List<ConexaoSlave> basicos;
    private List<ConexaoSlave> especiais;

    private ServidorMaster servidorMaster;
    private ExecutorService executorService;

    public ServidorMasterController(){
        executorService = Executors.newFixedThreadPool(1);
    }

    public void sair(){
        int answer = JOptionPane.showConfirmDialog(null, "Quer mesmo sair?",
                "Fechando o Conexão com segurança...", JOptionPane.YES_NO_OPTION);

        if (answer == JOptionPane.YES_OPTION) {
            desligarServidor();
            executorService.shutdown();
            System.exit(0);
        }
        return;
    }

    public void desligarServidor() {
        if(servidorMaster != null){
            try {
                servidorMaster.closeServer();
                servidorMaster = null;
                System.out.println("Servidor Master Desligado!");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    public void iniciarServidor(String porta){
        if(porta.isEmpty()){
            throw new PortaVaziaException("Digite uma porta para Iniciar Servidor!");
        }else {
            try {
                servidorMaster = new ServidorMaster(Integer.parseInt(porta), basicos, especiais);
                executorService.execute(servidorMaster);
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    public void setBasicos(List<ConexaoSlave> basicos) {
        this.basicos = basicos;
    }

    public void setEspeciais(List<ConexaoSlave> especiais) {
        this.especiais = especiais;
    }

    public ServidorMaster getServidorMaster() {
        return servidorMaster;
    }




}
