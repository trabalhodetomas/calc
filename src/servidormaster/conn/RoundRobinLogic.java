package servidormaster.conn;

import base.conn.Conexao;

import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by nti on 24/04/2017.
 */
public class RoundRobinLogic implements Runnable{

    private static final AtomicInteger systemIndex = new AtomicInteger(1);
    private Queue<ConexaoSlave> output = new ConcurrentLinkedDeque<>();

    private List<ConexaoSlave> conexaoSlaves;

    public RoundRobinLogic(List<ConexaoSlave> conexaoSlaves){
        this.conexaoSlaves = conexaoSlaves;
    }

    public Queue<ConexaoSlave> getOutput() {
        return output;
    }

    @Override
    public void run()
    {
        ConexaoSlave conexaoSlave = conexaoSlaves.get(systemIndex.incrementAndGet() % conexaoSlaves.size());
        System.out.printf("" + systemIndex.get());
        output.add(conexaoSlave);
    }

}
