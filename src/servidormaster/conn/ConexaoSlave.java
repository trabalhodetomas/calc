package servidormaster.conn;

import base.conn.AbstractConexao;

import java.io.IOException;

/**
 * Created by Pedro Felipe on 18/04/2017.
 */
public class ConexaoSlave extends AbstractConexao {

    public ConexaoSlave(String host, int porta) throws IOException {
        super(host, porta);
        iniciaConn();

    }


}
