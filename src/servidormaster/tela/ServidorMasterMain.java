package servidormaster.tela;

import base.exception.CampoVazioException;
import servidormaster.conn.ConexaoSlave;
import servidormaster.controller.ServidorMasterController;
import servidormaster.server.ServidorMaster;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Pedro Felipe on 24/04/2017.
 */
public class ServidorMasterMain extends JFrame {
    private JTextField txtServerPorta;
    private JTextField textSlaveHost1;
    private JTextField textSlavePorta1;
    private JButton btnIniciarServidor;
    private JTextField textSlaveHost2;
    private JTextField textSlaveHost3;
    private JTextField textSlaveHost4;
    private JTextField textSlavePorta2;
    private JTextField textSlavePorta3;
    private JTextField textSlavePorta4;
    private JPanel panel;

    private ServidorMasterController controller;
    private List<ConexaoSlave> basicos = new ArrayList<>();
    private List<ConexaoSlave> especiais = new ArrayList<>();


    public ServidorMasterMain(String title) {
        super(title);
        setContentPane(panel);

        controller = new ServidorMasterController();


        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent we) {
                controller.sair();
            }

        });
        btnIniciarServidor.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (controller.getServidorMaster() == null || controller.getServidorMaster().getServerSocket().isClosed()) {
                    iniciarSlaves();
                    controller.iniciarServidor(txtServerPorta.getText());
                    btnIniciarServidor.setText("Desligar");
                } else {
                    controller.desligarServidor();
                    btnIniciarServidor.setText("Iniciar Servidor");
                }
            }
        });
    }

    public void iniciarSlaves() {

        int porta1 = Integer.parseInt(textSlavePorta1.getText());
        int porta2 = Integer.parseInt(textSlavePorta2.getText());
        int porta3 = Integer.parseInt(textSlavePorta3.getText());
        int porta4 = Integer.parseInt(textSlavePorta4.getText());

        try {
            ConexaoSlave slave1 = new ConexaoSlave(textSlaveHost1.getText(), porta1);
            ConexaoSlave slave2 = new ConexaoSlave(textSlaveHost2.getText(), porta2);
            basicos.add(slave1);
            basicos.add(slave2);

            ConexaoSlave slave3 = new ConexaoSlave(textSlaveHost3.getText(), porta3);
            ConexaoSlave slave4 = new ConexaoSlave(textSlaveHost4.getText(), porta4);
            especiais.add(slave3);
            especiais.add(slave4);

            controller.setBasicos(basicos);
            controller.setEspeciais(especiais);
        } catch (IOException e) {
            e.printStackTrace();
            throw new CampoVazioException("Erro! Verifique todas as Conexões Slaves");
        }

    }


    public static void main(String[] args) {
        ServidorMasterMain frame = new ServidorMasterMain("Servidor Master");

        frame.pack();
        frame.setVisible(true);
    }


}