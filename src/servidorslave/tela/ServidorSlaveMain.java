package servidorslave.tela;

import servidorslave.controller.ServidorSlaveController;
import servidorslave.server.Logs;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * Created by Pedro Felipe on 19/04/2017.
 */
public class ServidorSlaveMain extends JFrame {

    private JPanel panel1;
    private JTextField txt_porta;
    private JButton btn_conectar;
    private JTextArea console;

    private ServidorSlaveController controller;

    public ServidorSlaveMain(String title) {
        setContentPane(panel1);

        controller = new ServidorSlaveController();

        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent we) {
                controller.sair();
            }

        });
        btn_conectar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (controller.getServidorSlave() == null || controller.getServidorSlave().getServerSocket().isClosed()) {
                    String r = controller.iniciarServidor(txt_porta.getText());
                    if(r != null) console.append("\r\nIniciando Servidor \t= " + r);
                    btn_conectar.setText("Desligar");
                } else {
                    String r = controller.desligarServidor();
                    if(r != null) console.append("\r\nDesligando Servidor \t= " + r);
                    btn_conectar.setText("Iniciar Servidor");
                }
            }
        });
    }

    public static void main(String[] args) {
        ServidorSlaveMain frame = new ServidorSlaveMain("ServidorSlave");

        frame.pack();
        frame.setResizable(false);
        frame.setVisible(true);
    }

}
