package servidorslave.server;

import base.util.VerificaCalculo;
import servidorslave.tela.ServidorSlaveMain;

import java.io.*;
import java.net.Socket;

/**
 * Created by Pedro Felipe on 19/04/2017.
 */
public class SocketSlave implements Runnable {

    private Socket socket;

    private DataInputStream in;
    private DataOutputStream out;

    public SocketSlave(Socket socket) throws IOException{
        this.socket = socket;
    }

    public String recebe() throws IOException{
        in = new DataInputStream(socket.getInputStream());
        return in.readUTF();
    }

    public void envia(String msg) throws IOException{
        if(!socket.isClosed()){
            out = new DataOutputStream(socket.getOutputStream());
            out.writeUTF(msg);
        }
    }

    private String calcular(String calculo){
        String[] numero = calculo.split(String.format("\\%s", VerificaCalculo.getOperador(calculo)));

        float r = 0;

        switch (VerificaCalculo.getOperador(calculo)) {
            case "+":
                r = Float.parseFloat(numero[0]) + Float.parseFloat(numero[1]);
                break;

            case "-":
                r = Float.parseFloat(numero[0]) - Float.parseFloat(numero[1]);
                break;

            case "*":
                r = Float.parseFloat(numero[0]) * Float.parseFloat(numero[1]);
                break;

            case "/":
                r = Float.parseFloat(numero[0]) / Float.parseFloat(numero[1]);
                break;

            case "√":
                r = (float) Math.sqrt(Float.parseFloat(numero[0]));
                break;

            case "%":
                r = Float.parseFloat(numero[0]) / 100;
                break;
        }
        return limpaResposta(r).replaceAll("\\.", ",");

    }

    private String limpaResposta(float r){
        String resposta = String.valueOf(r);
        if(resposta.endsWith(".0")){
            resposta = resposta.substring(0,resposta.length()-2);
        }
        return resposta;
    }

    @Override
    public void run() {
        try {
            String calculo = "";

            while(socket.isConnected() && !calculo.equalsIgnoreCase("Sair")) {

                calculo = recebe();
                System.out.println(calculo);
                if(calculo.equalsIgnoreCase("Teste")){
                    envia("OK");
                }else{
                    if (!calculo.isEmpty() && !calculo.equalsIgnoreCase("Sair"))
                        envia(calcular(calculo.replaceAll(",", ".")));
                }

            }
            if(calculo.equalsIgnoreCase("Sair")) {
                close();
                System.out.println("Cliente " + socket.getRemoteSocketAddress() + " Saiu!");
            }
        } catch (IOException e) {

            e.printStackTrace();
        }
    }

    public void close() throws IOException {
        envia("Sair");

        in.close();
        out.close();
        socket.close();
    }
}
