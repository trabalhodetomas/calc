package servidorslave.server;

/**
 * Created by nti on 12/05/2017.
 */
public class Logs {

    private static String log;

    public static void setLog(String log) {
        Logs.log = log;
    }

    public static String print() {
        return log;
    }
}
