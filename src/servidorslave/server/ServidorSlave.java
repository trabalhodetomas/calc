package servidorslave.server;

import base.conn.Conexao;
import base.server.AbstractServidor;
import com.sun.deploy.util.SessionState;

import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Pedro Felipe on 19/04/2017.
 */
public class ServidorSlave extends AbstractServidor implements Runnable {

    private List<SocketSlave> clientes;

    private ExecutorService executorService;
    private boolean canRun = true;

    public ServidorSlave(int porta) throws IOException {

        super(porta);
        executorService = Executors.newFixedThreadPool(4);
        clientes = new ArrayList<>();
        iniciaServer();
    }


    @Override
    public void run() {
        System.out.println("Servidor Slave no ar..");
        while(canRun){
            Socket cliente = null;
            try {
                System.out.println("Aguardando conexao...");
                cliente = getServerSocket().accept();

                System.out.println("Cliente conectado: " + cliente.getRemoteSocketAddress());
                SocketSlave socketCliente = new SocketSlave(cliente);
                clientes.add(socketCliente);

                executorService.execute(socketCliente);


            } catch (Exception e) {
                canRun = false;
            }
        }
    }

    @Override
    public void closeServer() throws IOException {
        for(SocketSlave cliente : clientes){
            cliente.close();
        }
        if(executorService != null){
            executorService.shutdown();
        }
        canRun = false;

        super.closeServer();


    }


    public List<SocketSlave> getClientes() {
        return clientes;
    }
}
