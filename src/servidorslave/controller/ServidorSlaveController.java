package servidorslave.controller;

import base.exception.PortaVaziaException;
import servidorslave.server.ServidorSlave;
import servidorslave.server.SocketSlave;

import javax.swing.*;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Pedro Felipe on 19/04/2017.
 */
public class ServidorSlaveController {

    private ServidorSlave servidorSlave;

    private ExecutorService executorServiceServer;

    public ServidorSlaveController(){
        executorServiceServer = Executors.newFixedThreadPool(1);
    }

    public void sair(){
        int answer = JOptionPane.showConfirmDialog(null, "Quer mesmo sair?",
                "Fechando o Conexão com segurança...", JOptionPane.YES_NO_OPTION);

        if (answer == JOptionPane.YES_OPTION) {
            desligarServidor();
            executorServiceServer.shutdown();

            System.exit(0);
        }
        return;
    }

    public String iniciarServidor(String porta) {

        String r = "";

        try {
            servidorSlave = new ServidorSlave(Integer.parseInt(porta));
            executorServiceServer.execute(servidorSlave);
            r = "Ok";
        } catch (Exception e) {
            e.printStackTrace();
            if(porta.isEmpty()){
                r = "Digite uma porta para Iniciar Servidor!";
                throw new PortaVaziaException(r);
            }
        }finally {
            return r;
        }


    }

    public ServidorSlave getServidorSlave() {
        return servidorSlave;
    }

    public String desligarServidor() {
        String r = "Falhou";

        try {
            if(servidorSlave != null){
                servidorSlave.closeServer();
                servidorSlave = null;
            }
            r = "Ok";
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            return r;
        }


    }
}
