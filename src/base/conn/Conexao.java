package base.conn;

import java.io.IOException;

/**
 * Created by Pedro Felipe on 18/04/2017.
 */
public interface Conexao{

    public void iniciaConn() throws IOException;

    public void closeConn() throws IOException;

    public void enviar(String msg) throws IOException;

    public String receber() throws IOException;

    public String testeConexao(String msg) throws IOException;

}
