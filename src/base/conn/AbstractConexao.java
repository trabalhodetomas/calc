package base.conn;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

/**
 * Created by Pedro Felipe on 18/04/2017.
 */
public abstract class AbstractConexao implements Conexao {

    private Socket conn;

    private String host;
    private int porta;

    private DataInputStream in;
    private DataOutputStream out;

    public AbstractConexao(String host, int porta){
        this.host = host;
        this.porta = porta;
    }

    @Override
    public void iniciaConn() throws IOException {

        conn = new Socket(host, porta);
        conn.setReuseAddress(true);
        conn.setTcpNoDelay(true);
        out = new DataOutputStream(conn.getOutputStream());
        in = new DataInputStream(conn.getInputStream());

    }

    @Override
    public void closeConn() throws IOException {

        if(conn!=null && conn.isConnected()) {
            if(conn.isClosed()){
                iniciaConn();
            }
            enviar("Sair");
            in.close();
            out.close();
            conn.close();
        }
    }

    @Override
    public void enviar(String msg) throws IOException {
        if(!conn.isConnected()){
            iniciaConn();

        }
        out.writeUTF(msg);

    }

    @Override
    public String receber() throws IOException{
        if(!conn.isConnected()){
            iniciaConn();
        }
        return in.readUTF();
    }

    public String testeConexao(String msg) throws IOException {

        if(!conn.isConnected()){
            iniciaConn();
        }
        enviar(msg);

        return receber();
    }


}
