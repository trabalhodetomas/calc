package base.server;

import java.io.IOException;
import java.net.ServerSocket;

/**
 * Created by Pedro Felipe on 18/04/2017.
 */
public abstract class AbstractServidor implements Servidor{

    private ServerSocket serverSocket;
    private int porta;

    public AbstractServidor(int porta){
        this.porta = porta;
    }

    public void iniciaServer() throws IOException {
        serverSocket = new ServerSocket(porta);
    }

    public void closeServer() throws IOException{
        if(serverSocket != null && !serverSocket.isClosed()){
            serverSocket.close();
        }
    }

    @Override
    public ServerSocket getServerSocket() {
        return serverSocket;
    }
}
