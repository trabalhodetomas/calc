package base.server;

import java.io.IOException;
import java.net.ServerSocket;

/**
 * Created by Pedro Felipe on 18/04/2017.
 */
public interface Servidor {

    public void iniciaServer() throws IOException;

    public void closeServer() throws IOException;

    public ServerSocket getServerSocket();

}
