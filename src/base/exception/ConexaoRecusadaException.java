package base.exception;

import javax.swing.*;

/**
 * Created by INSS on 19/04/2017.
 */
public class ConexaoRecusadaException extends RuntimeException {
     public ConexaoRecusadaException(String s) {
        super(s);

        JLabel lblMessage = new JLabel(s);
        Object[] texts = { lblMessage };
        JOptionPane.showMessageDialog(null, texts);
    }
}

