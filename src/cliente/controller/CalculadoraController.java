package cliente.controller;

import base.conn.Conexao;
import base.exception.CampoVazioException;
import base.exception.ConexaoRecusadaException;
import base.exception.PortaVaziaException;
import cliente.conn.ConexaoSocket;

import javax.swing.*;
import java.io.IOException;

/**
 * Created by Pedro Felipe on 18/04/2017.
 */
public class CalculadoraController {

    private JTextField txtHost;
    private JTextField txtPorta;

    private Conexao conexao;

    public String configuraConexao() {
        dialogConexao();
        String resposta = "";
        try {
            if(conexao != null){
                conexao.closeConn();
                conexao = null;
            }
            conexao = new ConexaoSocket(txtHost.getText(), Integer.parseInt(txtPorta.getText()));
            conexao.iniciaConn();
            resposta = conexao.testeConexao("Teste");
            System.out.println(resposta);
            if(resposta.equalsIgnoreCase("OK")){

                dialogTesteOK();
            }

        } catch (IOException e) {
            e.printStackTrace();
            resposta = "Falhou";
            if(txtHost.getText().isEmpty() || txtPorta.getText().isEmpty()){
                throw new CampoVazioException("Por favor, digite o host e a porta para conectar!");
            }else{
                throw new ConexaoRecusadaException("Não foi possivel estabelecer conexão! \r\n" + " Tente novamente!");
            }
        }finally {
            return resposta;
        }

    }

    private void dialogConexao() {
        JLabel lblMessage = new JLabel("Digite o Host e a Porta do Servidor Master!");
        txtHost = new JTextField("127.0.0.1");
        txtPorta = new JTextField("12345");
        Object[] texts = {lblMessage, txtHost, txtPorta};
        JOptionPane.showMessageDialog(null, texts);
    }

    private void dialogTesteOK() {
        JLabel lblMessage = new JLabel("Conectado com sucesso!");
        Object[] texts = {lblMessage};
        JOptionPane.showMessageDialog(null, texts);
    }


    public void sair(){
        int answer = JOptionPane.showConfirmDialog(null, "Quer mesmo sair?",
                "Fechando o Conexão com segurança...", JOptionPane.YES_NO_OPTION);

        if (answer == JOptionPane.YES_OPTION) {
            try {
                if(conexao != null)
                    conexao.closeConn();
            } catch (IOException e) {

            }
            System.exit(0);
        }
        return;
    }

    public String calcular(String calculo){
        String resultado = null;
        try {

            while (resultado == null) {
                if (!calculo.isEmpty()) {
                    conexao.enviar(calculo);
                }
                resultado = conexao.receber();
            }

        } catch (IOException e) {
            resultado = "0";
            e.printStackTrace();
            throw new ConexaoRecusadaException("Não foi possivel responder sua solicitação! \r\n" + " Tente novamente!");
        }finally {
            return resultado;
        }

    }

    public String actionBtn0(String display) {

        if (!"0".equals(display)) {
            display += "0";
        }
        return display;
    }
}
