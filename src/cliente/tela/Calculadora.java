package cliente.tela;

import base.util.VerificaCalculo;
import cliente.controller.CalculadoraController;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
 * Created by Pedro Felipe on 18/04/2017.
 */
public class Calculadora extends JFrame {
    private JPanel panel1;
    private JTextField display;
    private JButton btn_7;
    private JButton btn_1;
    private JButton btn_2;
    private JButton btn_3;
    private JButton btn_4;
    private JButton btn_5;
    private JButton btn_6;
    private JButton btn_point;
    private JButton btn_8;
    private JButton btn_9;
    private JButton btn_adicao;
    private JButton btn_subtracao;
    private JButton btn_mult;
    private JButton btn_0;
    private JButton btn_div;
    private JButton btn_limpar;
    private JButton btn_porc;
    private JButton btn_raiz;
    private JButton btn_result;
    private JTextArea console;
    private JButton reconectarButton;
    private JButton sairButton;

    private String teste;


    private CalculadoraController controller;

    public Calculadora(String title) {
        super(title);
        setContentPane(panel1);



        controller = new CalculadoraController();
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent we) {
                controller.sair();
            }

        });

        teste = controller.configuraConexao();
        if (teste != null) console.append("Teste de Conexão \t= " + teste);





        btn_0.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                display.setText(controller.actionBtn0(display.getText()));
            }
        });
        btn_1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if ("0".equals(display.getText())) {
                    display.setText("1");
                } else {
                    display.setText(display.getText() + 1);
                }
            }
        });
        btn_2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if ("0".equals(display.getText())) {
                    display.setText("2");
                } else {
                    display.setText(display.getText() + 2);
                }
            }
        });
        btn_3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if ("0".equals(display.getText())) {
                    display.setText("3");
                } else {
                    display.setText(display.getText() + 3);
                }
            }
        });
        btn_4.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if ("0".equals(display.getText())) {
                    display.setText("4");
                } else {
                    display.setText(display.getText() + 4);
                }
            }
        });
        btn_5.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if ("0".equals(display.getText())) {
                    display.setText("5");
                } else {
                    display.setText(display.getText() + 5);
                }
            }
        });
        btn_6.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if ("0".equals(display.getText())) {
                    display.setText("6");
                } else {
                    display.setText(display.getText() + 6);
                }
            }
        });
        btn_7.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if ("0".equals(display.getText())) {
                    display.setText("7");
                } else {
                    display.setText(display.getText() + 7);
                }
            }
        });
        btn_8.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if ("0".equals(display.getText())) {
                    display.setText("8");
                } else {
                    display.setText(display.getText() + 8);
                }
            }
        });
        btn_9.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if ("0".equals(display.getText())) {
                    display.setText("9");
                } else {
                    display.setText(display.getText() + 9);
                }
            }
        });
        btn_limpar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                display.setText("0");
            }
        });
        btn_point.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String calculo = display.getText();
                if (VerificaCalculo.verificaOperador(calculo)) {
                    String[] cs = calculo.split(String.format("\\%s", VerificaCalculo.getOperador(calculo)));
                    if (calculo.endsWith(VerificaCalculo.getOperador(calculo))) {
                        display.setText(display.getText() + "0,");
                    } else if (cs[1] != null) {
                        if (!cs[1].contains(",")) {
                            display.setText(display.getText() + ",");
                        }
                    }
                } else {
                    if (!calculo.contains(",")) {
                        display.setText(display.getText() + ",");
                    }
                }
            }
        });
        btn_adicao.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String calculo = display.getText();

                if (VerificaCalculo.verificaOperador(calculo)) {
                    if (calculo.endsWith(VerificaCalculo.getOperador(calculo))) {
                        calculo += calculo.substring(0, calculo.length() - 1);
                        String resposta = controller.calcular(calculo);
                        console.append("\n"+calculo + "\t= " + resposta);
                        display.setText(resposta);
                    }
                } else {
                    if (calculo.endsWith(",")) {
                        display.setText(calculo.substring(0, calculo.length() - 1) + "+");
                    } else {
                        display.setText(calculo + "+");
                    }
                }

            }
        });
        btn_subtracao.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String calculo = display.getText();

                if (VerificaCalculo.verificaOperador(calculo)) {
                    if (calculo.endsWith(VerificaCalculo.getOperador(calculo))) {
                        calculo += calculo.substring(0, calculo.length() - 1);
                        String resposta = controller.calcular(calculo);
                        console.append("\n"+calculo + "\t= " + resposta);
                        display.setText(resposta);
                    }
                } else {
                    if (calculo.endsWith(",")) {
                        display.setText(calculo.substring(0, calculo.length() - 1) + "-");
                    } else {
                        display.setText(calculo + "-");
                    }
                }
            }
        });
        btn_mult.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String calculo = display.getText();

                if (VerificaCalculo.verificaOperador(calculo)) {
                    if (calculo.endsWith(VerificaCalculo.getOperador(calculo))) {
                        calculo += calculo.substring(0, calculo.length() - 1);
                        String resposta = controller.calcular(calculo);
                        console.append("\n"+calculo + "\t= " + resposta);
                        display.setText(resposta);
                    }
                } else {
                    if (calculo.endsWith(",")) {
                        display.setText(calculo.substring(0, calculo.length() - 1) + "*");
                    } else {
                        display.setText(calculo + "*");
                    }
                }
            }
        });
        btn_div.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String calculo = display.getText();

                if (VerificaCalculo.verificaOperador(calculo)) {
                    if (calculo.endsWith(VerificaCalculo.getOperador(calculo))) {
                        calculo += calculo.substring(0, calculo.length() - 1);
                        String resposta = controller.calcular(calculo);
                        console.append("\n"+calculo + "\t= " + resposta);
                        display.setText(resposta);
                    }
                } else {
                    if (calculo.endsWith(",")) {
                        display.setText(calculo.substring(0, calculo.length() - 1) + "/");
                    } else {
                        display.setText(calculo + "/");
                    }
                }
            }
        });
        btn_result.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String calculo = display.getText();
                if (calculo != "0") {
                    if (VerificaCalculo.verificaOperador(calculo)) {
                        if (calculo.endsWith(",")) {
                            calculo = calculo.substring(0, calculo.length() - 1);
                        }
                        if (calculo.endsWith(VerificaCalculo.getOperador(calculo))) {
                            calculo += calculo.substring(0, calculo.length() - 1);
                        }
                        String resposta = controller.calcular(calculo);
                        console.append("\n"+calculo +"\t= " + resposta);
                        display.setText(resposta);
                    }
                }
            }
        });
        btn_raiz.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String calculo = display.getText();
                if (calculo != "0") {
                    if(!VerificaCalculo.verificaOperador(calculo)){
                        if (calculo.endsWith(",")) {
                            calculo = calculo.substring(0, calculo.length() - 1);
                        }

                        String resposta = controller.calcular(calculo + "√");
                        console.append("\n"+ "√" + calculo +"\t= " + resposta);
                        display.setText(resposta);
                    }
                }
            }
        });

        btn_porc.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String calculo = display.getText();
                if (calculo != "0") {
                    if(!VerificaCalculo.verificaOperador(calculo)){
                        if (calculo.endsWith(",")) {
                            calculo = calculo.substring(0, calculo.length() - 1);
                        }

                        String resposta = controller.calcular(calculo + "%");
                        console.append("\n"+ calculo +"% \t= " + resposta);
                        display.setText(resposta);
                    }
                }
            }
        });

        reconectarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                teste = controller.configuraConexao();
                if (teste.equalsIgnoreCase("Ok"))
                    console.append("\nTeste de Conexão \t= " + teste);
                else
                    console.append("\nTeste de Conexão \t= " + "Falhou");
            }
        });
        sairButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                controller.sair();
            }
        });


    }



    public static void main(String[] args) {
        Calculadora app = new Calculadora("Calculadora Distribuida");
        app.pack();

        app.setMinimumSize(new Dimension(298, 400));
        app.setResizable(false);
        app.setVisible(true);
    }

}